# Fiap Netflix

>  Documentação do trabalho - Case Netflix para o Trabalho de Conclusão da Disciplina na disciplina Services Architecture / API / Mobile Architecture - Prof.: Tadeu D’Alessandro Barbosa



## InstalaçãoPré-Requisitos:
* GitBash
* Docker Client / Docker Desktop com suporte  a Docker Compose

 
## Passo a Passo :

* Realize o clone do seguinte repositório:
```sh
git clone https://gitlab.com/gabrielmunizlima/Fiap-Netflix.git
```
* Acesse o diretório “Infra-Compose”
```sh
cd Infra-Compose
```
* Execute o comando para iniciar a stack:
```sh
docker-compose up --build
```
*OBS*: A aplicação poderá demorar cerca de 1 a 2 minutos para que todos os recursos fiquem prontos.
