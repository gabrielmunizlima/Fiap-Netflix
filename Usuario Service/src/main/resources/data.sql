--popular tabelas
INSERT INTO USUARIO (nome) VALUES ('Bruno Cavalcante');
INSERT INTO USUARIO (nome) VALUES ('Felipe Gonzalez');
INSERT INTO USUARIO (nome) VALUES ('Gabriel Muniz');

INSERT INTO ASSISTIDOS (idUsuario, idFilme) VALUES (1, 1);
INSERT INTO ASSISTIDOS (idUsuario, idFilme) VALUES (1, 2);
INSERT INTO ASSISTIDOS (idUsuario, idFilme) VALUES (2, 1);
INSERT INTO ASSISTIDOS (idUsuario, idFilme) VALUES (2, 2);
INSERT INTO ASSISTIDOS (idUsuario, idFilme) VALUES (3, 1);
INSERT INTO ASSISTIDOS (idUsuario, idFilme) VALUES (3, 2);

INSERT INTO INTERESSE (idUsuario, idFilme) VALUES (1, 1);
INSERT INTO INTERESSE (idUsuario, idFilme) VALUES (1, 2);
INSERT INTO INTERESSE (idUsuario, idFilme) VALUES (2, 1);
INSERT INTO INTERESSE (idUsuario, idFilme) VALUES (2, 2);
INSERT INTO INTERESSE (idUsuario, idFilme) VALUES (3, 1);
INSERT INTO INTERESSE (idUsuario, idFilme) VALUES (3, 2);