package com.fiap.netflix.filme.service;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fiap.netflix.filme.exception.ObjectNotFoundException;
import com.fiap.netflix.filme.model.Votacao;
import com.fiap.netflix.filme.repository.FilmeRepository;
import com.fiap.netflix.filme.repository.VotacaoRepository;
import com.fiap.netflix.filme.vo.Nota;

@Service
public class VotacaoService {

	@Autowired
	private VotacaoRepository votacaoRepo;

	@Autowired
	private FilmeRepository filmeRepo;

	public Nota votar(Votacao votacao) {
		verificarExistenciaFilme(votacao.getIdFilme());
		votacaoRepo.save(votacao);
		return obterNota(votacao.getIdFilme());
	}

	public Nota obterNota(long idFilme) {
		verificarExistenciaFilme(idFilme);
		return new Nota(votacaoRepo.findByIdFilme(idFilme));
	}

	private void verificarExistenciaFilme(long id) {
		if (!filmeRepo.findById(id).isPresent())
			throw new ObjectNotFoundException(MessageFormat.format("O filme de id {0} não foi localizado!", id));

	}

}
