package com.fiap.netflix.filme.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import com.fiap.netflix.filme.service.GeneroService;
import com.fiap.netflix.filme.vo.VOGeneroFilmeMaisVisto;
import com.fiap.netflix.filme.model.Genero;

@RestController
@RequestMapping("/generos")
public class GeneroController {

	@Autowired
	private GeneroService generoService;

	@GetMapping
	public ResponseEntity<List<Genero>> obterGeneros() {
		return new ResponseEntity<>(generoService.consultarTodos(), HttpStatus.OK);

	}

	@GetMapping("/top")
	public ResponseEntity<List<VOGeneroFilmeMaisVisto>> obterFilmesMaisVistos() {

		return new ResponseEntity<>(generoService.obterFilmemaisVistoPorGenero(), HttpStatus.OK);

	}

}
