package com.fiap.netflix.filme.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fiap.netflix.filme.vo.Nota;

@Entity
public class Filme {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String titulo;
    @Column(columnDefinition="TEXT")
	private String descricao;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Genero genero;
	private long qtdeVisualizacoes;
	@Column(name = "tags")
	@ElementCollection(targetClass = String.class)
	private List<String> tags;
	@Transient
	private Nota nota;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public long getQtdeVisualizacoes() {
		return qtdeVisualizacoes;
	}

	public void setQtdeVisualizacoes(long qtdeVisualizacoes) {
		this.qtdeVisualizacoes = qtdeVisualizacoes;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Nota getNota() {
		return nota;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

}
