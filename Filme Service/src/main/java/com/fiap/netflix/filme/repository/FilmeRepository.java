package com.fiap.netflix.filme.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fiap.netflix.filme.model.Filme;

public interface FilmeRepository extends JpaRepository<Filme, Long> {

	@Query("select f from Filme f inner join f.genero g where g.nome = :nome")
	public List<Filme> consultarPorGenero(String nome);

	public Filme findByTitulo(String titulo);

	public List<Filme> findByTagsIn(java.util.Set<String> tags);

	@Query(value = "select f.* from Filme f  where f.genero_id = :id order by f.qtdeVisualizacoes desc limit 1", nativeQuery = true)
	public Filme maisVistoPorGenero(long id);

}
