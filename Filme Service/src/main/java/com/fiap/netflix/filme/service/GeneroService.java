package com.fiap.netflix.filme.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fiap.netflix.filme.model.Genero;
import com.fiap.netflix.filme.repository.FilmeRepository;
import com.fiap.netflix.filme.repository.GeneroRepository;
import com.fiap.netflix.filme.vo.VOGeneroFilmeMaisVisto;

@Service
public class GeneroService {

	@Autowired
	private GeneroRepository generoRepo;
	@Autowired
	private FilmeRepository filmeRepo;

	public List<Genero> consultarTodos() {
		return generoRepo.findAll();
	}

	public List<VOGeneroFilmeMaisVisto> obterFilmemaisVistoPorGenero() {
		List<VOGeneroFilmeMaisVisto> maisVistos = new ArrayList<>();

		List<Genero> generos = consultarTodos();

		for (Genero genero : generos) {
			VOGeneroFilmeMaisVisto maisVisto = new VOGeneroFilmeMaisVisto(genero,
					filmeRepo.maisVistoPorGenero(genero.getId()));
			maisVistos.add(maisVisto);
		}

		return maisVistos;
	}

}
