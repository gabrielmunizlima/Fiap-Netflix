package com.fiap.netflix.filme.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fiap.netflix.filme.exception.ObjectNotFoundException;
import com.fiap.netflix.filme.model.Votacao;
import com.fiap.netflix.filme.service.VotacaoService;
import com.fiap.netflix.filme.vo.MensagemErro;


@RestController
@RequestMapping("/filmes/{id}/nota")
public class VotacaoController {

	@Autowired
	private VotacaoService votacaoService;

	// TODO o objeto de votacao receberia o id do usuario via token, mas como não
	// aplicamos a camada de autenticação
	// será aguardado o valor via body
	@PostMapping
	ResponseEntity<Object> votar(@PathVariable long id, @RequestBody Votacao votacao) {
		try {
			votacao.setIdFilme(id);
			return new ResponseEntity<>(votacaoService.votar(votacao), HttpStatus.OK);
		} catch (ObjectNotFoundException e) {
			return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping
	ResponseEntity<Object> obterNota(@PathVariable long id) {
		try {
			return new ResponseEntity<>(votacaoService.obterNota(id), HttpStatus.OK);
		} catch (ObjectNotFoundException e) {
			return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

}
