package com.fiap.netflix.filme.vo;

import java.util.List;

import com.fiap.netflix.filme.model.Votacao;

public class Nota {

	private long pontuacaoMedia;
	private long qtdVotacoes;

	public Nota(List<Votacao> votacoes) {
		this.qtdVotacoes = votacoes.size();

		if (qtdVotacoes <= 0)
			return;

		long total = 0;
		for (Votacao v : votacoes) {
			total += v.getPontuacao();
		}
		this.pontuacaoMedia = total / this.qtdVotacoes;
	}

	public long getPontuacaoMedia() {
		return pontuacaoMedia;
	}

	public void setPontuacaoMedia(long pontuacaoMedia) {
		this.pontuacaoMedia = pontuacaoMedia;
	}

	public long getQtdVotacoes() {
		return qtdVotacoes;
	}

	public void setQtdVotacoes(long qtdVotacoes) {
		this.qtdVotacoes = qtdVotacoes;
	}

}
