package com.fiap.netflix.filme.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fiap.netflix.filme.model.Filme;
import com.fiap.netflix.filme.repository.FilmeRepository;

@Service
public class FilmeService {

	@Autowired
	private FilmeRepository filmeRepo;

	public List<Filme> consultarPorGenero(String nomeGenero) {
		return filmeRepo.consultarPorGenero(nomeGenero);
	}

	public Filme consultarPorId(long id) {
		return filmeRepo.findById(id).get();
	}

	public List<Filme> consultarTodos() {
		return filmeRepo.findAll();
	}

	
	public Filme consultarPorTitulo(String titulo) {
		return filmeRepo.findByTitulo(titulo);
	}

	public List<Filme> consultarPorTags(List<String> tags) {
		Set<String> hSet = new HashSet<String>();
		for (String x : tags)
			hSet.add(x);
		return filmeRepo.findByTagsIn(hSet);
	}
}
